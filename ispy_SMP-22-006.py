import FWCore.ParameterSet.Config as cms

process = cms.Process('ISPY')

process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

from Configuration.AlCa.GlobalTag_condDBv2 import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data', '')

process.source = cms.Source('PoolSource',
                            fileNames = cms.untracked.vstring(

#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/0D10752B-4D58-BF4A-96D0-F9616AE06B72.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/4F904215-91A1-9B4B-9A08-34D1F634A0ED.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/695C26C9-9675-3F40-A672-40410D67D112.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/A1C1FA8C-6943-9248-8C15-50BEF29C5A3F.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/A4A489B6-F161-9B45-83BF-A08B19D7DA58.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/A7694FA0-425D-584B-99C3-317DFFFB908A.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/BAD409EF-68D6-5E44-BFFC-2C3E2046C54D.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/C6AFAF1A-0263-8648-9576-168120F31797.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/CF6D7F71-6D7E-054B-8C63-9891AD7DC8CE.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/E7C38F9C-C4BF-2240-BC2C-F347A42BA4FE.root',
'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/F52EA63D-8B36-C24E-82C3-C42524ECB6BC.root',
#'/store/data/Run2018D/MuonEG/AOD/15Feb2022_UL2018-v1/2520001/FC191BAC-BD32-CB4D-9FC8-534D566BF38C.root'    
),
             eventsToProcess = cms.untracked.VEventRange(
    '322252:1709622381',
    )
                            )
  
from FWCore.MessageLogger.MessageLogger_cfi import *

process.add_(
    cms.Service('ISpyService',
    outputFileName = cms.untracked.string('SMP-22-006_AOD_1709622381.ig'),
    outputIg = cms.untracked.bool(True),
    outputMaxEvents = cms.untracked.int32(25),  
    debug = cms.untracked.bool(False)
    )
)

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1) 
)

process.load("ISpy.Analyzers.ISpyEvent_cfi")
process.load('ISpy.Analyzers.ISpyEBRecHit_cfi')
process.load('ISpy.Analyzers.ISpyEERecHit_cfi')
process.load('ISpy.Analyzers.ISpyESRecHit_cfi')
process.load('ISpy.Analyzers.ISpyHBRecHit_cfi')
process.load('ISpy.Analyzers.ISpyHERecHit_cfi')
process.load('ISpy.Analyzers.ISpyHFRecHit_cfi')
process.load('ISpy.Analyzers.ISpyHORecHit_cfi')
process.load('ISpy.Analyzers.ISpyMuon_cfi')
process.load('ISpy.Analyzers.ISpyPFJet_cfi')
process.load('ISpy.Analyzers.ISpyPFMET_cfi')
process.load('ISpy.Analyzers.ISpyVertex_cfi')
process.load('ISpy.Analyzers.ISpyTrackExtrapolation_cfi')
process.load('ISpy.Analyzers.ISpyTriggerEvent_cfi')
process.load('ISpy.Analyzers.ISpyTrack_cfi')

process.ISpyEBRecHit.iSpyEBRecHitTag = cms.InputTag('reducedEcalRecHitsEB')
process.ISpyEERecHit.iSpyEERecHitTag = cms.InputTag('reducedEcalRecHitsEE')
process.ISpyESRecHit.iSpyESRecHitTag = cms.InputTag('reducedEcalRecHitsES')

process.ISpyHBRecHit.iSpyHBRecHitTag = cms.InputTag("reducedHcalRecHits:hbhereco")
process.ISpyHERecHit.iSpyHERecHitTag = cms.InputTag("reducedHcalRecHits:hbhereco")
process.ISpyHFRecHit.iSpyHFRecHitTag = cms.InputTag("reducedHcalRecHits:hfreco")
process.ISpyHORecHit.iSpyHORecHitTag = cms.InputTag("reducedHcalRecHits:horeco")

process.ISpyMuon.iSpyMuonTag = cms.InputTag("muons")

#process.ISpyPFJet.iSpyPFJetTag = cms.InputTag('ak4PFJets')
process.ISpyPFJet.iSpyPFJetTag = cms.InputTag('pfJetsEI')
process.ISpyPFJet.etaMax = cms.double(3.0)

process.ISpyPFMET.iSpyPFMETTag = cms.InputTag("pfMet")

process.ISpyTrackExtrapolation.iSpyTrackExtrapolationTag = cms.InputTag("trackExtrapolator")
process.ISpyTrackExtrapolation.trackPtMin = cms.double(0.0)

process.ISpyTrack.ptMin = cms.double(0.0)
process.ISpyTrack.isRECO = cms.bool(False)

process.out = cms.OutputModule(
    "PoolOutputModule",
    fileName = cms.untracked.string("SMP-22-006_AOD.root")
)

process.iSpy = cms.Path(process.ISpyEvent*
                        process.ISpyEBRecHit*
                        process.ISpyEERecHit*
                        process.ISpyESRecHit*
                        process.ISpyHBRecHit*
                        process.ISpyHERecHit*
                        process.ISpyHFRecHit*
                        process.ISpyHORecHit*
                        process.ISpyMuon*
                        process.ISpyPFJet*
                        process.ISpyPFMET*
                        process.ISpyVertex*
                        process.ISpyTrackExtrapolation)

#process.schedule = cms.Schedule(process.iSpy)
process.outpath = cms.EndPath(process.out)





